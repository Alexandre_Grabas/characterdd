package fr.eni.passion.imh;

import fr.eni.passion.bll.PersonneManager;
import fr.eni.passion.bo.Personne;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/passion")
public class ControllerPassion {

    @Autowired
    private PersonneManager personneManager;

    @GetMapping
    public String salut(Model model){
        model.addAttribute("personne",personneManager.getAll());
        return "passion";
    }

    @GetMapping("/add")
    public String add(Personne personne){
        return "add";
    }

    @PostMapping("/add")
    public String addPost(@Valid Personne personne, BindingResult errors){
        personneManager.addPersonne(personne);
        return "redirect:/passion";
    }

}
