package fr.eni.passion.bll;

import fr.eni.passion.bo.Personne;
import fr.eni.passion.dal.PersonneDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonneManagerImpl implements PersonneManager{
    @Autowired
    PersonneDao personneDao;

    @Override
    public List<Personne> getAll() {
        return (List<Personne>) personneDao.findAll();
    }

    @Override
    public Personne addPersonne(Personne personne) {
        return personneDao.save(personne);
    }

    @Override
    public String deletePersonne(Integer id) {
        if (personneDao.existsById(id)) {
            personneDao.deleteById(id);
            return "Youpi";
        }else{
            return "Nulpi";
        }


    }


}
