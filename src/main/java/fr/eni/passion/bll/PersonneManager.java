package fr.eni.passion.bll;

import fr.eni.passion.bo.Personne;

import java.util.List;

public interface PersonneManager {

    public List<Personne> getAll();

    public Personne addPersonne(Personne personne);

    public String deletePersonne(Integer id);

}
