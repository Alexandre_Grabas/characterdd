package fr.eni.passion.ws;

import fr.eni.passion.bll.PersonneManager;
import fr.eni.passion.bo.Personne;
import fr.eni.passion.dal.PersonneDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/caracter")
public class ClasseWS {

    @Autowired
    PersonneManager personneManager;

    @CrossOrigin
    @GetMapping("/all")
    public List<Personne> getCaracters(){
        return personneManager.getAll();
    }

    @CrossOrigin
    @PostMapping("/create")
    public Personne create(@RequestBody Personne personne){
        return personneManager.addPersonne(personne);
    }

    @CrossOrigin
    @PutMapping("/update/{id}")
    public Personne update (@PathVariable Integer id, @RequestBody Personne personne){
        return personneManager.getAll().get(0);
    }

    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Integer id){
        return personneManager.deletePersonne(id);
    }

    @GetMapping("/classe")
    public List<String> classe(){
        List<String> listeClasse = new ArrayList<>(Arrays.asList("Magicien", "Bard", "Paladin", "Moine", "Barbare", "Guerrier", "Roublard", "Druid", "Clerc", "Ensorceleur", "Occultiste", "Rodeur"));
        return listeClasse;
    }

    @GetMapping("/expertise")
    public List<String> expertise(){
        List<String> listeExpertise = new ArrayList<>(Arrays.asList("Escamotage","Athlétisme","Arcane","Acrobaties","Discrétion","Investigation","Histoire","Nature","Religion","Dressage","Intuition","Médecine","Perception","Survie","Intimidation","Persuasion","Représentation","Tromperie"));
        return listeExpertise;
    }

    @GetMapping("/race")
    public List<String> race(){
        List<String> listeRace = new ArrayList<>(Arrays.asList("Humain","Elfe","Demi-Elfe","Nain","Gythianki","Demi-Orc","Draw","Tiflyn","Gnome","Halflin"));
        return listeRace;
    }

    @GetMapping("/sexe")
    public List<String> sexe(){
        List<String> listeSexe = new ArrayList<>(Arrays.asList("M","F"));
        return listeSexe;
    }

    @GetMapping("/test")
    public String test(){

        List<String> ls;
        return "bleé";
    }

}
