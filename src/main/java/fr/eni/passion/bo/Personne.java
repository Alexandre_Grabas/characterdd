package fr.eni.passion.bo;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
public class Personne {
    @Id
    @GeneratedValue
    private Integer id;
    private String nom;
    private String prenom;
    private Integer age;
    private String race;
    @ElementCollection
    private List<String> passions=new ArrayList<>();
    private String sexe;
    private String metier;
}
