package fr.eni.passion.dal;

import fr.eni.passion.bo.Personne;
import org.springframework.data.repository.CrudRepository;

public interface PersonneDao extends CrudRepository<Personne,Integer> {



}
